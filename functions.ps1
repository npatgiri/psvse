function Get-VseKids {
    param (
        # Dashboard Content
        [Parameter(Position = 0, Mandatory = $true)]
        [string]
        $Content
    )
    $allKids = New-Object System.Collections.ArrayList
    $TextInfo = (Get-Culture).TextInfo
    $null = $Content -match ("<form.*>.*?<\/form>")
    $databoardForms = $Matches[0]
    $kids = $databoardForms | Select-String -Pattern "<option.*?/option>" -AllMatches | ForEach-Object { $_.Matches.Value }
    Write-Information "Number of Kids $($kids.count)"
    foreach ($item in $kids) {
        $tempKid = New-Object psobject
        $Name = $item -replace (".*value=(.*?)>(.*)<.*", '$2')
        $Name = $TextInfo.ToTitleCase($Name.tolower()).Trim()
        $list = [ordered]@{
            ID   = $item -replace (".*value=(.*?)>(.*)<.*", '$1')
            Name = $Name
        }
        $tempKid | Add-Member -NotePropertyMembers $list
        $allKids.Add($tempKid) | Out-Null
    }
    $allKids
}

function Get-VseFile {
    if ($list.link -ne "" -and $list.Download) {
        $random = Join-Path $folder $($(Get-Random).ToString())
        $downloadFile = Invoke-WebRequest -WebSession $vydehi -OutFile $random -Uri $list.link -Resume -PassThru
        Write-Information "Downloading - $($list.Description)"
        $fileExt = $downloadFile.Headers['Content-Disposition'] -replace (".*(\..*)", '$1')
        $filename = $list.Filename + $fileExt
        Write-Information "Creating file - $filename"
        $null = Move-Item $random "$folder\$filename" -PassThru -Force
        if ($config.emailSent -eq $true) {
            Send-vseEmail
        }
    }
}
function Send-vseEmail {
    [string[]]$to = $config.emailSetting.emailTo.split(",")
    [securestring]$secStringPassword = ConvertTo-SecureString $config.emailSetting.emailPassword -AsPlainText -Force
    [pscredential]$cred = New-Object System.Management.Automation.PSCredential ($config.emailSetting.emailFrom, $secStringPassword)
    $mailBody = ""
    $mailBody += '<b>Kid:</b>&emsp; ' + $list.Kid + '<br>'
    $mailBody += '<b>Type:</b>&emsp; ' + $list.Type + '<br>'
    $mailBody += '<b>Title:</b>&emsp; ' + $list.Title + '<br>'
    $mailBody += '<b>Description:</b>&emsp; ' + $list.Description + '<br>'
    $mailBody += '<b>Attachment:</b>&emsp; ' + $filename + '<br>'
    $Subject = $list.Kid.Split(" ")[0] + "-" + $list.Title
    $mailsplat = @{
        To         = $to
        From       = $config.emailSetting.emailFrom
        Body       = $mailBody
        Subject    = $Subject
        SmtpServer = $config.emailSetting.smtpServer
        Port       = $config.emailSetting.smtpPort
        UseSsl     = $config.emailSetting.smtpUseSsl
        Credential = $cred
        Attachment = "$folder\$filename"
    }
    Send-MailMessage @mailsplat -BodyAsHtml
}
function New-Folder {
    param (
        # Folder Object
        [Parameter(Mandatory = $true, Position = 0)]
        $FolderName
    )
    if(Test-Path $FolderName){
        Convert-Path $FolderName
    }
    else {
        $null = New-Item -ItemType Directory $FolderName
        Convert-Path $FolderName
    }
}
