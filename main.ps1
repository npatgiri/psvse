#!/app/powershell/pwsh
$ProgressPreference = "SilentlyContinue"
$InformationPreference = "Continue"
$TextInfo = (Get-Culture).TextInfo

$scriptPath = $PSScriptRoot -eq "" ? '.' :$PSScriptRoot
Import-Module "$scriptPath\functions.ps1" -Force
if(Test-Path "$scriptPath\temp\config.jsonc"){
    $config = Get-Content "$scriptPath\temp\config.jsonc" | ConvertFrom-Json
}
else{
    $config = Get-Content "$scriptPath\config.jsonc" | ConvertFrom-Json
}

$storeLocation = New-Folder $config.storeLocation

$username = [System.Web.HTTPUtility]::UrlEncode($config.vseUsername)
$password = [System.Web.HTTPUtility]::UrlEncode($config.vsePassword)
$mainBody = "username_temp=$username&username=$username&password=$password"

if (Test-Path "$storeLocation\report.json") {
    [System.Collections.ArrayList]$mainReport = Get-Content "$storeLocation\report.json" | ConvertFrom-Json -ErrorAction SilentlyContinue
}

# Base URL
$baseURL = "https://vydehischool.ecoleaide.com/"
$login = Invoke-WebRequest 'https://vydehischool.ecoleaide.com/home.htm' -Method 'POST' -Body $mainBody -ContentType "application/x-www-form-urlencoded" -SessionVariable vydehi
$dashboard = Invoke-WebRequest 'https://vydehischool.ecoleaide.com/dashboard.htm' -WebSession $vydehi

# Get Dashboard ID
$allKids = Get-VseKids $dashboard.Content
$allAlerts = New-Object System.Collections.ArrayList

foreach ($k in $allKids) {
    Write-Information "Looking for alerts for $($k.Name)"
    $urlEncodeID = [System.Web.HTTPUtility]::UrlEncode($k.ID)
    $body = "parentStudents=$urlEncodeID"
    $kidName = $TextInfo.ToTitleCase(($k.name -split "\s")[0].ToLower())
    $folder = Join-Path $storeLocation $kidName 
    $null = New-Folder $folder
    $dashboard = Invoke-WebRequest 'https://vydehischool.ecoleaide.com/dashboard.htm' -WebSession $vydehi -Method Post -ContentType "application/x-www-form-urlencoded" -Body $body
    $minify = $dashboard.Content -replace ("\n", "")
    
    $events = $minify -match "<!--First column-->(.*)<!--End first column-->"

    if ($events) {
        $eventList = $Matches[1] | Select-String -Pattern "<div class='alert.*?>.*?<\/div>.*?<\/div>" -AllMatches | ForEach-Object { $_.Matches.Value }
        foreach ($item in $eventList) {
            $eventReport = New-Object psobject
            $hyperlink = ""
            $title = $item -Replace (".*?h5>(.*?)\s*</h5>.*", '$1')
            if ($item -match (".*href='(.*?)'.*")) {
                $hyperlink = $baseURL + $($item -Replace (".*href='(.*?)'.*", '$1'))
                $download = $title -in $mainReport.Title ? $false : $true
            } 
            $list = [ordered]@{
                Kid         = $k.Name
                Type        = "Event"
                Title       = $title
                Description = $item -Replace (".*?<p>(.*?)<\/p>.*", '$1')
                link        = $hyperlink
                Filename    = $item -Replace (".*?h5>(.*?)\s*</h5>.*", '$1') -replace (".*:\s*(.*)\s*", '$1') -replace ("[^a-zA-Z0-9 -]", "")
                Download    = $download
            }
            $eventReport | Add-Member -NotePropertyMembers $list
            $allAlerts.Add($eventReport) | Out-Null
            Get-VseFile
        }        
    }
    $alert = $minify -match "<!--Second column-->(.*)<!--End second column-->"

    if ($alert) {
        $alertList = $Matches[1] | Select-String -Pattern "<div class='alert.*?>.*?<\/div>.*?<\/div>" -AllMatches | ForEach-Object { $_.Matches.Value }
        foreach ($item in $alertList) {
            $alertReport = New-Object psobject
            $hyperlink = ""
            $title = $item -Replace (".*h5>(.*?)\s*</h5>.*", '$1')
            if ($item -match (".*href='(.*?)'.*")) {
                $hyperlink = $baseURL + $($item -Replace (".*href='(.*?)'.*", '$1'))
                $download = $title -in $mainReport.Title ? $false : $true
            } 
            $list = [ordered]@{
                Kid         = $k.Name
                Type        = "Alert"
                Title       = $title
                Description = $item -Replace (".*?<p>(.*?)<\/p>.*", '$1')
                link        = $hyperlink
                Filename    = $item -Replace (".*h5>(.*?)\s*</h5>.*", '$1') -replace (".*:\s*(.*)\s*", '$1') -replace ("[^a-zA-Z0-9 -]", "")
                Download    = $download
            }
            $alertReport | Add-Member -NotePropertyMembers $list
            $allAlerts.Add($alertReport) | Out-Null
            Get-VseFile
        }
    }
}
$date = Get-Date -Format yyyyMMdd
$null = New-Folder "$storeLocation\report\"
$allAlerts | ConvertTo-Json | Out-File $("$storeLocation\report\" + $date + ".json")
$allAlerts | ConvertTo-Json | Out-File $("$storeLocation\report.json")