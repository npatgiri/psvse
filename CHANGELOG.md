# v2021.7.1

* Enhancement - Move downloaded content to data folder by default
* Enhancement - Add downloaded content location to config file
* Remove - Deleted Send-VSEmail function
* Fix - Email body 
* Enhancement - Developer convenience: If temp\conf.jsonc exist which is on gitignore, it will pickup instead of root config.jsonc file
* New - Added New-Folder function

# v2021.6.2

* Enhancement - Allow SMTP outside outlook.com as well as per config file
* New - Added Send-vseEmail

# v2021.6.1

* New - Create config file for storing user info instead of hard coded in script
* New - Add ability to enable/disable email
* New - Add email settings to config file
