# About

VSE stands for [Vydehi School of Excellence](https://www.vydehischool.com/). The school offers a parent portal for the kids who study in their school. It is full of information. Although it is great without the availability of push notification or emails, we parents needs regular visits to the portal. Over a year I missed few circular and only came to know post the due date. To overcome the problem, the PowerShell script came into existence. 

> **_NOTE:_**  This is not official from Vydehi. I built it for my own convenience. Happy if it helps anyone else as well. NO GUARANTEES :)

# Requirement

One needs [PowerShell Core](https://aka.ms/powershell-release?tag=stable) which can be installed on Windows, Linux or a Mac.

# How does it work

* Install PowerShell
* Download script or clone the repo
* Configure the config.jsonc file with your information
* Execute main.ps1 in PowerShell.

# Additional Note

* The code is tested on Windows and Linux. 
* This is also tested on a Raspberry Pi 3 on crontab schedule